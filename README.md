# Django Channels

Channels extend Django abilities beyond HTTP to handle protocols that require long-running connections such as Web Sockets, chat protocol, and more.

## Synchronous vs Asynchronous

* Synchronous is an operation that perform one task at a time, it moves to the next task after it completes the previous one. 

* Django web pages are mostly synchronous, they depend on web synchronous request that sends a request (HTTP) and wait for a reply (HTTP 200, HTTP 404, etc) in order to processed to the next step.

* Asynchrouns operation allows you to move to the next task before the previous one finishes.

* In chat applications, you can send a message (request) to a certain user without waiting for a reply from the server or the user to make another request (send another message). In synchronous manner, we will not receive a message unless we make a HTTP request.

https://www.outsystems.com/blog/posts/asynchronous-vs-synchronous-programming/


## Web Sockets

* Web Socket (WS) is a full-deplex and bi-directional protocol that creates an environment where the client and the server communicate to each other independently at any time.

* In a chatroom situation, User (a) sends a request (message) to the server, and the server will upgrade the HTTP request to become a WS request. Next, a persistent connection is established so that the server and the User (a) can push messages and close the connection at anytime. The same logic will be applied for the new users that join the chatroom.

* The synchronous operations build upon WSGI (Web Server Gateway Interface) support and they are rooted to Django views. 

* Channels provide asynchrounus extension that build upon ASGI (Asynchronous Server Gateway Interface) support and rooted to channels consumers (channels version of Django views).

## Channels

Channels manage the independent communication between the client and the server. In chat programs, channels create a room for any conversation happens between different users. User (a) can send a message that is visible as well to the User (b) and other users if we have a group conversation. 






